#!/usr/bin/python
# coding=utf-8
# ------------------------
# VUT FIT 2BIA Brno
# Jakub Kubik (xkubik32)
# interpret for IPPcode18
# ------------------------

# TODO DATOVY TYP STRING (ESCAPE SEKVENCIE + NEPRIPUSTNE HODNODTY space, # , \) 

from parsexml import *
from interpretClasses import *
import sys

# ########## Global variables required for interpretation
globalFrame = Frame()       # creating object for global frame
tempFrame = Frame()         # creating object for temporary frame
locFrame = Frame()          # creating object for locale frame
frameStack = []             # stack for local frames (after every instruction will be actualized if req)
isTempFr = False            # if i am now in temporary frame 
isLocFr = False             # if i am now in locale frame

dataStack = []              # stack for data (2 instruction)
returnIndexStack = []       # stack for indexes of instruction which to use when return is called
allLabels = {}              # creating object for all labels
ActInstr = 'nop'            # ActInstr must ge global because i need to change it in CALL and RET and JMPS instrs
InstrList = []              # Instruction list
indexOfActInstr = 0         # index of actual instruction
isInstrCallRetJmp = False

def checkScopeAndFindVar(instrName, var):
    """ Function for checking if variable object is in actual frame """
    frameName = var[:3]
    global globalFrame
    global locFrame
    global tempFrame   
    global isTempFr
    global isLocFr 

    if frameName == 'LF@':
        # check if loc frame is available
        if isLocFr:
            # check if variable is in loc frame
            if not locFrame.findVar(var):
                sys.stderr.write(instrName + ": local frame exist but variable isnt there\n")
                sys.exit(54)
            else:
                return locFrame.getVar(var)
        # loc frame isnt available
        sys.stderr.write(instrName + ': bad scope local farme doesnt exist\n')
        sys.exit(55)

    elif frameName == 'TF@':
        # check if temp frame is available
        if isTempFr:
            # check if variable is in temp frame
            if not tempFrame.findVar(var):
                sys.stderr.write(instrName + ': temporary frame exists but variable isnt there\n')
                sys.exit(54)
            # variable is in frame so return it
            return tempFrame.getVar(var)
        # temp frame isnt available
        sys.stderr.write(instrName + ': bad scope temp farme doesnt\n')
        sys.exit(55)

    elif frameName == 'GF@':
        # check if variable is in global frame
        if not globalFrame.findVar(var):
            sys.stderr.write(instrName + ': global frame exists but variable isnt there ' + var + '\n')
            sys.exit(54)
        # variable is in global frame
        return globalFrame.getVar(var)
        

def insertVarToRightFrame(var):
    """ Insert variable to right frame acording var name """
    global globalFrame
    global tempFrame
    global locFrame
    global frameStack

    if var.name[:3] == 'GF@':
        globalFrame.insertVar(var)

    elif var.name[:3] == 'TF@':
        tempFrame.insertVar(var)
    
    elif var.name[:3] == 'LF@':
        locFrame.insertVar(var)
        frameStack.pop()
        frameStack.append(locFrame)


def checkArithmeticOp(instrName, position):
    """ check one operand in arithmetic operation (add, sub, mul, idiv) """
    if ActInstr.argsType[position] == 'var':
        retVar = checkScopeAndFindVar(instrName, ActInstr.args[position])
        if retVar.dataType == 'nondef':
            sys.stderr.write(instrName + ': ' + str(position) +  ' operand is var and has undef value\n')
            sys.exit(56)
        elif retVar.dataType != 'int':
            sys.stderr.write(instrName + ': ' + str(position) +  ' operand is var but isnt int\n')
            sys.exit(53)
        return int(retVar.value)

    elif ActInstr.argsType[position] != 'int':
        sys.stderr.write(instrName + ': ' + str(position) +  ' operand isnt int\n')
        sys.exit(53)
    return int(ActInstr.args[position])


def checkRelationalOp(instr, position):
    """ check one operand in reational operation (lt, gt, eq) """
    if ActInstr.argsType[position] == 'var':
        op = checkScopeAndFindVar(instr, ActInstr.args[position])
    else:
        value = ippcodeTypeToPythType(ActInstr.argsType[position], ActInstr.args[position])
        op = Variable('op' + str(position), ActInstr.argsType[position], value)
    return op
    


def checkLogicalOp(instr, position):
    """ check one operand in logical operation (and, or, not) """
    if ActInstr.argsType[position] == 'var':
        op = checkScopeAndFindVar(instr, ActInstr.args[position])
        if op.dataType != 'bool':
            sys.stderr.write(instr +  ' symb' + str(position) + ' je premenna a nie je typu bool\n')
            sys.exit(53)
    else:
        value = ippcodeTypeToPythType(ActInstr.argsType[position], ActInstr.args[position])
        op = Variable('op' + str(position), ActInstr.argsType[position], value)
        if  op.dataType != 'bool':
            sys.stderr.write(instr + 'op' + str(position) + ' je literal a nie je typu bool')
            sys.exit(53)
    return op


def checkStringOp(instr, position):
    """ check one operand in string operation (int2char, stri2int, concat, strlen, getchar, setchar) """
    if ActInstr.argsType[position] == 'var':
        op = checkScopeAndFindVar(instr, ActInstr.args[position])
    else:
        value = ippcodeTypeToPythType(ActInstr.argsType[position], ActInstr.args[position])
        op = Variable('op' + str(position), ActInstr.argsType[position], value)
    return op


def checkJumpOp(instr, position):
    """ check one operand in jump operation (jump, jumpifeq, jumpifneq) """
    if ActInstr.argsType[position] == 'var':
        op = checkScopeAndFindVar(instr, ActInstr.args[position])
    else:
        value = ippcodeTypeToPythType(ActInstr.argsType[position], ActInstr.args[position])
        op = Variable('op' + str(position), ActInstr.argsType[position], value)
    return op

def replace(match):
    return chr(int(match.group(1)))

def ippcodeTypeToPythType(dType, val):
    """ convert ippcode data to python data value """
    if dType == 'bool':
        if val == 'true':
            return True
        elif val == 'false':
            return False
    
    elif dType == 'int':
        return int(val)
    
    elif dType == 'string':
        
        if (' ' in val) == True or ('#' in val) == True:
            sys.stderr.write('in string is space or #\n')
            sys.exit(32)
        regex = re.compile(r"\\(\d{1,3})")
        new = regex.sub(replace, val)
        
        return new

def main():
    """ Main of whole program """
    global globalFrame
    global tempFrame
    global locFrame
    global frameStack
    global isTempFr
    global isLocFr
    global dataStack
    global returnIndexStack
    global allLabels
    global ActInstr
    global InstrList
    global indexOfActInstr
    global isInstrCallRetJmp

    # ########## Parsing XML file and creating Instruction List
    progArgs = ParseProgramArgsAndOpenFile()    # create object which for parsing XML
    fd = progArgs.openFile()                    # get file descriptor of opened file
    parseFile = ParseXML()                      # parse xml and create instruction list
    parseFile.parseXMLFile(fd)                  # get file descriptor of opened file 

    InstrList = parseFile.getInstrLis()         # get created instruction list

    # creating asoc array of labels
    allLabels = AllLabels(InstrList) # create label object for jumping thoroug instruction acording labe
    # allLabels.printAllLabels()
    allLabels.checkLabelsRedefinition(InstrList)    # check labels redefinition from whole code

    # ##### Creating frames and all important data structures for interpreting
    ActInstr = InstrList[0] # normal variable
    InstrList.append(None)
    
    # interpreting instruction   
    while ActInstr is not None:
        #print(indexOfActInstr)
        asocInstrFunc[ActInstr.name]()

        # Move to the next instruction
        if not isInstrCallRetJmp:
            indexOfActInstr = indexOfActInstr + 1
            ActInstr = InstrList[indexOfActInstr]
        isInstrCallRetJmp = False
    
    # printing helpful information ####################################################
    #print('\nGlobalny ramec')
    #globalFrame.printVarLabelDict()

    #if isTempFr:
    #    print('\nDocasny ramec')
    #    tempFrame.printVarLabelDict()
    
    #if len(frameStack) > 0:
    #    localFrame = frameStack.pop()
    #    print('\nLokalny ramenc')
    #    localFrame.printVarLabelDict()


# ##### FUNCTIONS FOR ALL INSTRUCTIONS #####################################################

def move():
    """ MOVE <var> <symb> """
    checkScopeAndFindVar('MOVE', ActInstr.args[0])  # <var>

    if ActInstr.argsType[1] == 'var':   # check if <symb> is var
        retVar = checkScopeAndFindVar('MOVE', ActInstr.args[1])
        var = Variable(ActInstr.args[0], retVar.dataType, retVar.value)
    
    else:   # <symb> is literal
        value = ippcodeTypeToPythType(ActInstr.argsType[1], ActInstr.args[1]) # convert string to data type
        var = Variable(ActInstr.args[0], ActInstr.argsType[1], value)

    # insert var to right frame
    insertVarToRightFrame(var)

    # print('move')


def createframe():
    """ CREATEFRAME """
    global tempFrame
    global isTempFr
    tempFrame = Frame() 
    isTempFr = True   

    # print('createframe')


def pushframe():
    """ PUSHFRAME """
    global locFrame
    global frameStack
    global isLocFr
    global isTempFr
    # check if temp frame is available
    if not isTempFr:
        sys.stderr.write('PUSHFRAME: access to undefined temp frame\n')
        sys.exit(55)
    
    # create local frame
    locFrame = Frame()
    
    # copy all varaibles from temp frame to loc frame
    for key, var in tempFrame.varsDict.items():
        name = 'LF@' + key[3:]
        var.name = name
        locFrame.insertVar(var)
    
    # add loc frame to stack frame
    frameStack.append(locFrame)
    isLocFr = True             
    isTempFr = False           
    
    # print('pushframe')


def popframe():
    """ POPFRAME """
    global tempFrame
    global isTempFr
    global isLocFr
    global frameStack
    # check if frame stack isnt empty
    if len(frameStack) == 0:
        sys.stderr.write('Stack for frames is empty\n')
        exit(55)

    isTempFr = True   
    isLocFr = False   
    tempFrame = Frame() 

    # copy all variables from loc frame to temp frame
    localFrame = frameStack.pop()
    for key, var in localFrame.varsDict.items():
        name = 'TF@' + key[3:]
        var.name = name
        tempFrame.insertVar(var)

    # print('popframe')


def defvar():
    """ DEFVAR <var> """ 
    global globalFrame
    global locFrame
    global tempFrame
    global frameStack
    
    # ##### check global frame
    if ActInstr.args[0][:3] == 'GF@':
        if globalFrame.findVar(ActInstr.args[0][:3]):
            sys.stderr.write('DEFVAR: redefinition of variable in global frame\n')
            sys.exit(52)

        # inset to global frame nondef <var>
        var = Variable(ActInstr.args[0], 'nondef', '42')
        globalFrame.insertVar(var)
    
    # ##### check local frame
    elif ActInstr.args[0][:3] == 'LF@':
        if not isLocFr:
            sys.stderr.write('DEFVAR: access to nonexisting local frame\n')
            sys.exit(55)

        elif locFrame.findVar(ActInstr.args[0][:3]):
            sys.stderr.write('DEFVAR: redefinition of variable in local frame\n')
            sys.exit(52)

        # insert to local frame nondef <var>
        var = Variable(ActInstr.args[0], 'nondef', '42')
        locFrame.insertVar(var)
        frameStack.pop()
        frameStack.append(locFrame)

    # ##### check temporary frame
    elif ActInstr.args[0][:3] == 'TF@':
        if not isTempFr:
            sys.stderr.write('DEFVAR: access to nonexisting temporary frame\n')
            sys.exit(55)
        
        elif tempFrame.findVar(ActInstr.args[0][:3]):
            sys.stderr.write('DEFVAR: redefinition of variable in temp frame\n')
            sys.exit(52)

        # insert to temporary frame nondef <var>
        var = Variable(ActInstr.args[0], 'nondef', '42')
        tempFrame.insertVar(var)

    # print('defvar')


def call():
    """ CALL <label> """
    global ActInstr
    global isInstrCallRetJmp
    global returnIndexStack
    global InstrList
    global indexOfActInstr
    # check if label exist
    if not allLabels.findLabel(ActInstr.args[0]):
        sys.stderr.write('CALL: label doesnt exist\n')
        sys.exit(52)
    
    # set next instr index to return index stack 
    labelIndex = allLabels.getLabel(ActInstr.args[0]) # get index of label in inst linst
    ActInstr = InstrList[labelIndex]                  # set act instr
    returnIndexStack.append(indexOfActInstr+1)               # save next instr index after this instr call
    isInstrCallRetJmp = True
    indexOfActInstr = labelIndex

    # print('call')


def returnI():
    """ RETURN """ 
    global ActInstr
    global isInstrCallRetJmp
    global returnIndexStack
    global indexOfActInstr
    # check where to return 
    if len(returnIndexStack) == 0:
        sys.stderr.write('RETURN: Nowhere to return\n')
        sys.exit(56)

    # set ActInstr to return index instr from instr list
    indexOfActInstr = returnIndexStack.pop()
    ActInstr = InstrList[indexOfActInstr]
    isInstrCallRetJmp = True

    # print('return')


def pushs():
    """ PUSHS <symb> """
    global dataStack
    # <symb> is variable
    if ActInstr.argsType[0] == 'var':
        var = checkScopeAndFindVar('PUSHS', ActInstr.args[0])
        if var.dataType == 'nondef':
            sys.stderr.write('PUSHS: variable sah undefined value\n')
            sys.exit(56)
    
    # <symb> is literal
    else:
        value = ippcodeTypeToPythType(ActInstr.argsType[0], ActInstr.args[0])
        var = Variable('literal', ActInstr.argsType[0], value)
    
    # add value and data type to data stack
    dataStack.append(var)

    # print('pushs')


def pops():
    """ POPS <var> """
    global dataStack
    # check data stack size
    if len(dataStack) == 0:
        sys.stderr.write('POPS: data stack is empty\n')
        sys.exit(56)
    # check scope of <var>
    checkScopeAndFindVar('POPS', ActInstr.args[0])

    # remove value from data stack
    var = dataStack.pop()
    newVar = Variable(ActInstr.args[0], var.dataType, var.value)

    # add newVar to <var> frame
    insertVarToRightFrame(newVar)

    # print('pops')


def add():
    """ ADD <var> <symb1> <symb2> """
    checkScopeAndFindVar('ADD', ActInstr.args[0])   # <var>
    op1 = checkArithmeticOp('ADD', 1)               # <symb1>
    op2 = checkArithmeticOp('ADD', 2)               # <symb2>
    
    # count and save value to right frame acording <var>
    value = op1 + op2
    var = Variable(ActInstr.args[0], 'int', value)
    insertVarToRightFrame(var)

    # print('add')


def sub():
    """ SUB <var> <symb1> <symb2> """ 
    checkScopeAndFindVar('SUB', ActInstr.args[0])   # <var>
    op1 = checkArithmeticOp('SUB', 1)               # <symb1>
    op2 = checkArithmeticOp('SUB', 2)               # <symb2>

    # count and save value to right frame acording <var>
    value = op1 - op2
    var = Variable(ActInstr.args[0], 'int', value)
    insertVarToRightFrame(var) 

    # print('sub')


def mul():
    """ MUL <var> <symb1> <symb2> """ 
    checkScopeAndFindVar('MUL', ActInstr.args[0])   # <var>    
    op1 = checkArithmeticOp('MUL', 1)               # <symb1>
    op2 = checkArithmeticOp('MUL', 2)               # <symb2>

    # count and save value to right frame acording <var>
    value = op1 * op2
    var = Variable(ActInstr.args[0], 'int', value)
    insertVarToRightFrame(var) 

    # print('mul')


def idiv():
    """ IDIV <var> <symb1> <symb2> """ 
    checkScopeAndFindVar('IDIV', ActInstr.args[0])  # <var>
    op1 = checkArithmeticOp('IDIV', 1)              # <symb1>
    op2 = checkArithmeticOp('IDIV', 2)              # <symb2>

    # check division by 0 
    if op2 == 0:
        sys.stderr.write('IDIV: division by 0\n')
        sys.exit(57)

    # count and save value to right frame acording <var>
    value = op1 // op2
    var = Variable(ActInstr.args[0], 'int', value)
    insertVarToRightFrame(var)

    # print('idiv')


def lt():
    """ LT <var> <symb1> <symb2> """
    checkScopeAndFindVar('LT', ActInstr.args[0])  # <var>
    op1 = checkRelationalOp('LT', 1)              # <symb1>
    op2 = checkRelationalOp('LT', 2)              # <symb2>

    # check allowd data types
    if op1.dataType != op2.dataType:
        sys.stderr.write('LT: symb1 a symb2 nie su rovnakeho datoveho typu')
        sys.exit(53)
    
    # count and save value to right frame acording <var>
    value = op1.value < op2.value
    var = Variable(ActInstr.args[0], 'bool', value)
    insertVarToRightFrame(var)

    # print('lt')


def gt():
    """ GT <var> <symb1> <symb2> """
    checkScopeAndFindVar('GT', ActInstr.args[0])  # <var>
    op1 = checkRelationalOp('GT', 1)              # <symb1>
    op2 = checkRelationalOp('GT', 2)              # <symb2>

    # check allowed data types
    if op1.dataType != op2.dataType:
        sys.stderr.write('GT: symb1 a symb2 nie su rovnakeho datoveho typu')
        sys.exit(53)
    
    # count and save value to right frame acording <var>
    value = op1.value > op2.value
    var = Variable(ActInstr.args[0], 'bool', value)
    insertVarToRightFrame(var)

    # print('gt')


def eq():
    """ EQ <var> <symb1> <symb2> """
    checkScopeAndFindVar('EQ', ActInstr.args[0])  # <var>
    op1 = checkRelationalOp('EQ', 1)              # <symb1>
    op2 = checkRelationalOp('EQ', 2)              # <symb2>

    # check allowed data types
    #print(op1.dataType + ' ' + op2.dataType)
    if op1.dataType != op2.dataType:
        
        sys.stderr.write('EQ: symb1 a symb2 nie su rovnakeho datoveho typu')
        sys.exit(53)
    
    # count and save value to right frame acording <var>
    value = op1.value == op2.value
    var = Variable(ActInstr.args[0], 'bool', value)
    insertVarToRightFrame(var)

    # print('eq')


def andI():
    """ AND <var> <symb1> <symb2> """
    checkScopeAndFindVar('AND', ActInstr.args[0]) # <var>
    op1 = checkLogicalOp('AND', 1)                # <symb1>
    op2 = checkLogicalOp('AND', 2)                # <symb2>

    # count and save value to right frame acording <var>
    value = op1.value and op2.value
    var = Variable(ActInstr.args[0], 'bool', value)
    insertVarToRightFrame(var)

    # print('and')


def orI():
    """ OR <var> <symb1> <symb2> """
    checkScopeAndFindVar('OR', ActInstr.args[0]) # <var>
    op1 = checkLogicalOp('OR', 1)                # <symb1>
    op2 = checkLogicalOp('OR', 2)                # <symb2>

    # count and save value to right frame acording <var>
    value = op1.value or op2.value
    var = Variable(ActInstr.args[0], 'bool', value)
    insertVarToRightFrame(var)
    
    # print('or')


def notI():
    """ NOT <var> <symb> """
    checkScopeAndFindVar('NOT', ActInstr.args[0]) # <var>
    op = checkLogicalOp('NOT', 1)                # <symb>

    # count and save value to right frame acording <var>
    value = not op.value
    var = Variable(ActInstr.args[0], 'bool', value)
    insertVarToRightFrame(var)

    # print('not')


def int2char():
    """ INT2CHAR <var> <symb> """
    checkScopeAndFindVar('INT2CHAR', ActInstr.args[0]) # <var>
    op = checkStringOp('INT2CHAR', 1)                  # <symb>

    # check data type
    if op.dataType != 'int':
        sys.stderr.write('INT2CHAR: symb data type isnt int')
        sys.exit(53)

    # count and save value to right frame acorfing <var>
    try:
        value = chr(op.value)
    except:
        sys.stderr.write('INT2CHAR: not unciode value')
        sys.exit(58)
    var = Variable(ActInstr.args[0], 'str', value)
    insertVarToRightFrame(var)

    # print('int2char')


def stri2int():
    """ STRI2INT <var> <symb1> <symb2> """
    checkScopeAndFindVar('STRI2INT', ActInstr.args[0])
    op1 = checkStringOp('STRI2INT', 1)                  # <symb>
    op2 = checkStringOp('STRI2INT', 2)                  # <symb>

    # check data types and indexing
    if op1.dataType != 'string' or op2.dataType != 'int':
        sys.stderr.write('STRI2INT: symb1 or symb2 data type isnt ok')
        sys.exit(53)
    
    if len(op1.value) - 1 < op2.value or op2.value < 0:
        sys.stderr.write('STRI2INT: indexing mimo retazec\n')
        sys.exit(58)
    
    # count and save value to right frame acording <var>
    value = ord(op1.value[op2.value])
    var = Variable(ActInstr.args[0], 'int', value)
    insertVarToRightFrame(var)

    # print('stri2int')


def read():
    """ READ <var> <type> """
    checkScopeAndFindVar('READ', ActInstr.args[0])  # <var> 

     # get value acording <type> and save it to right frame
    try:
        value = input()            
    except:
        sys.stderr.write('ajok')

    if ActInstr.args[1] == 'int':
        try:
            value = int(value)
        except:
            value = 0
    elif ActInstr.args[1] == 'string':
        try:
            value = str(value)
        except:
            value = '\n'
    elif ActInstr.args[1] == 'bool':
        if value.lower() == 'true':
            value = True
        else:
            value = False

    var = Variable(ActInstr.args[0], ActInstr.args[1], value)
    insertVarToRightFrame(var)

    # print('read')


def write():
    """ WRITE <symb> """
    if ActInstr.argsType[0] == 'var':
        var = checkScopeAndFindVar('WRITE', ActInstr.args[0])
    else:
        value = ippcodeTypeToPythType(ActInstr.argsType[0], ActInstr.args[0])
        var = Variable('literal', ActInstr.argsType[0], value)
    
    if var.dataType == 'bool':
        if var.value == True:
            print('true')
        else :
            print('false')
    else:
        print(str(var.value))

    # print('write')


def concat():
    """" CONCAT <var> <symb1> <symb2> """
    checkScopeAndFindVar('CONCAT', ActInstr.args[0]) # <var>
    op1 = checkStringOp('CONCAT', 1)                 # <symb1>
    op2 = checkStringOp('CONCAT', 2)                 # <symb2>

    # check data type
    if op1.dataType != 'string' or op2.dataType != 'string':
        sys.stderr.write('CONCAT: symb1 or symb2 data type isnt string')
        sys.exit(53)
    
    # count and save value to right frame acording <var>
    value = op1.value + op2.value
    var = Variable(ActInstr.args[0], 'string', value)
    insertVarToRightFrame(var)

    # print('concat')


def strlen():
    """ STRLEN <var> <symb> """
    checkScopeAndFindVar('STRLEN', ActInstr.args[0]) # <var>
    op = checkStringOp('STRLEN', 1)                  # <symb>

    # check data type
    if op.dataType != 'string':
        sys.stderr.write('STRLEN: symb1 data type isnt str\n')
        sys.exit(53)
    
    # count and save value to right frame acording <var>
    value = len(op.value)
    var = Variable(ActInstr.args[0], 'int', value)
    insertVarToRightFrame(var)

    # print('strlen')


def getchar():
    """ GETCHAR <var> <symb1> <symb2> """
    checkScopeAndFindVar('GETCHAR', ActInstr.args[0]) # <var>
    op1 = checkStringOp('GETCHAR', 1)                 # <symb1>
    op2 = checkStringOp('GETCHAR', 2)                 # <symb2>

    # check data types and indexing
    if op1.dataType != 'string' or op2.dataType != 'int':
        sys.stderr.write('GETCHAR: symb1 or symb2 data type isnt ok')
        sys.exit(53)
    
    if len(op1.value) - 1 < op2.value or op2.value < 0:
        sys.stderr.write('GETCHAR dlzka retazca op1 je vacsia ako pozadovany index op2')
        sys.exit(58)

    # count and save value to right frame acording <var>
    value = op1.value[op2.value]
    var = Variable(ActInstr.args[0], 'string', value)
    insertVarToRightFrame(var)

    # print('getchar')


def setchar():
    """ SETCHAR <var> <symb1> <symb2> """
    var = checkScopeAndFindVar('SETCHAR', ActInstr.args[0]) # <var>
    op1 = checkStringOp('SETCHAR', 1)                 # <symb1>
    op2 = checkStringOp('SETCHAR', 2)                 # <symb2>

    # check data types and indexing
    if op1.dataType != 'int' or op2.dataType != 'string':
        sys.stderr.write('SETCHAR: first or second operand isnt OK')
        sys.exit(53)

    if var.dataType == 'nondef':
        sys.stderr.write('SETCHAR var has undefined value')
        sys.exit(58)
    if op2.value == '':
        sys.stderr.write('SETCHAR prazdny retazec')
        sys.exit(58)
    if var.dataType != "string":
        sys.stderr.write('SETCHAR bad data type\n')
        sys.exit(53)
    if len(var.value) - 1 < op1.value or op1.value < 0:
        sys.stderr.write('SETCHAR idexovanie mimo retazca')
        sys.exit(58)
    
    # count and save value to rigt frame acording <var>
    value = var.value[:op1.value] + op2.value[0] + var.value[(op1.value+1):]
    newVar = Variable(ActInstr.args[0], 'string', value)
    insertVarToRightFrame(newVar)

    # print('setchar')


def typeI():
    """ TYPE <var> <symb> """
    checkScopeAndFindVar('TYPE', ActInstr.args[0])  # <var>
    op1 = checkStringOp('SETCHAR', 1)               # <symb>

    # save value to right frame acorfing <var>
    if re.search(r'^[-+]?\d+$', str(op1.value)) :
        dataType = "int"
    elif re.search(r'^True|False$', str(op1.value)) :
        dataType = "bool"
    else:
        dataType = "string"
        
    var = Variable(ActInstr.args[0], 'setBool', dataType)
    insertVarToRightFrame(var)

    # print('type')


def label():
    """ LABEL <label> """
    # I have already implemented
    # print('label')


def jump():
    """ JUMP <label> """
    global isInstrCallRetJmp
    global indexOfActInstr
    global ActInstr

    # <label>
    if not allLabels.findLabel(ActInstr.args[0]):
        sys.stderr.write('JUMP: nonexisting label\n')
        sys.exit(52)
    
    # get jmp instruction index and set act instr
    label = allLabels.getLabel(ActInstr.args[0])
    ActInstr = InstrList[label]
    isInstrCallRetJmp = True
    indexOfActInstr = label

    # print('jump')


def jumpifeq():
    """ JUMPIFEQ <label> <symb1> <symb2> """
    global isInstrCallRetJmp
    global indexOfActInstr
    global ActInstr

    # <label>
    if not allLabels.findLabel(ActInstr.args[0]):
        sys.stderr.write('JUMPIFEQ: na neexistijuce navestie\n')
        sys.exit(52)
    label = allLabels.getLabel(ActInstr.args[0])
    op1 = checkJumpOp('JUMPIFEQ', 1)                 # <symb1>
    op2 = checkJumpOp('JUMPIFEQ', 2)                 # <symb2>

    # chcek data types
    if op1.dataType != op2.dataType:
        sys.stderr.write('JUMPIFEQ op1 data type isnt same as op2 data type')
        sys.exit(53)

    # check if i have to jump
    if op1.value == op2.value:
        ActInstr = InstrList[label]
        isInstrCallRetJmp = True
        indexOfActInstr = label

    # print('jumpifeq')


def jumpifneq():
    """ JUMPIFNEQ <label> <symb1> <symb2> """
    global isInstrCallRetJmp
    global indexOfActInstr
    global ActInstr

    # <label>
    if not allLabels.findLabel(ActInstr.args[0]):
        sys.stderr.write('JUMPIFEQ: na neexistijuce navestie\n')
        sys.exit(52)
    label = allLabels.getLabel(ActInstr.args[0])
    op1 = checkJumpOp('JUMPIFNEQ', 1)                 # <symb1>
    op2 = checkJumpOp('JUMPIFNEQ', 2)                 # <symb2>

    # chcek data types
    if op1.dataType != op2.dataType:
        sys.stderr.write('JUMPIFNEQ op1 data type isnt same as op2 data type')
        sys.exit(53)

    # check if i have to jump
    if op1.value != op2.value:
        ActInstr = InstrList[label]
        isInstrCallRetJmp = True
        indexOfActInstr = label

    # print('jumpifneq')


def dprint():
    """ DPRINT """
    pass
    # print('dprint')

def breakI():
    """ BREAK """
    pass
    # print('break')

asocInstrFunc = {
    'MOVE': move, 'CREATEFRAME': createframe, 'PUSHFRAME': pushframe, 'POPFRAME': popframe,
    'DEFVAR': defvar, 'CALL': call, 'RETURN':returnI, 'PUSHS': pushs, 'POPS': pops, 'ADD': add,
    'SUB': sub, 'MUL': mul, 'IDIV': idiv, 'LT': lt, 'GT': gt, 'EQ': eq, 'AND': andI,
    'OR': orI, 'NOT': notI, 'INT2CHAR': int2char, 'STRI2INT': stri2int, 'READ': read,
    'WRITE': write, 'CONCAT': concat, 'STRLEN': strlen, 'GETCHAR': getchar, 'SETCHAR': setchar,
    'TYPE': typeI, 'LABEL': label, 'JUMP': jump, 'JUMPIFEQ': jumpifeq,
    'JUMPIFNEQ': jumpifneq, 'DPRINT': dprint, 'BREAK': breakI
}


if __name__ == "__main__":
    main()