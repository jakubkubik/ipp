# ------------------------
# VUT FIT 2BIA Brno
# Jakub Kubik (xkubik32)
# interpret for IPPcode18
# ------------------------


import re # for regular expression
import sys
import getopt
import xml.etree.ElementTree as ET  
import array   


class Instruction:
    """ Simple class for predefined and parsed instructions saved in instruction list from XML """

    def __init__(self, name, len, *args):

        self.name = name
        self.len = len
        self.args = []
        for i in args:
            self.args.append(i)
    
    def argsType(self, argum):
        self.argsType = []
        for i in argum:
            self.argsType.append(i)


def usage():
        print('\n\t====================================================================')
        print('\t\tProgram načte XML reprezentaci programu ze zadaného ')
        print('\t\tsouboru a tento program s využitím standardního vstupu')
        print('\t\tvstupu a výstupu interpretuje.')
        print('\t=====================================================================\n')

class ParseProgramArgsAndOpenFile:
    """ Simple class for parsing program arguemnts """
    
    def __init__(self):
        if len(sys.argv) != 2: 
            sys.stderr.write('Wrong number of program arguemnts\n')
            sys.exit(10)

        try:
            opts, args = getopt.getopt(sys.argv[1:], "", ["help", "source="])
        except getopt.GetoptError as err:
            sys.stderr.write("\n\tError in program arguments\n")
            # usage()
            sys.exit(10)
        for o, a in opts:
            if o == "--help":
                usage()
                sys.exit(0)
            elif o == "--source":
                self.filename = a
            else:
                sys.stderr.write('Wrong program arguments\n')
                sys.exit(10)
    
    def openFile(self):
        try:
            self.myfile = open(self.filename, "r+") # or "a+", whatever you need
        except IOError:
            sys.stderr.write("Could not open file!\n")
            sys.exit(11)    # correct return vale
        return self.myfile



class ParseXML:
    """ Class for parsing whole XML file """

    def __init__(self):

        # checking data types 
        self.varRegex = r'^var$'
        self.labelRegex = r'^label$'
        self.symbRegex = r'(^var$)|(^int$)|(^bool$)|(^string$)'
        self.typeRegex = r'^type$'


        # checking conrete values of data type
        self.var = r'^(GF@|LF@|TF@)[a-zA-Z\_\-\$\&\%\*][a-zA-Z0-9\_\-\$\&\%\*]*$'
        self.label = r'^[a-zA-Z\_\-\$\&\%\*][a-zA-Z0-9\_\-\$\&\%\*]*$'
        self.integer = r'^[-+]?\d+$'
        self.string = r'.+'
        self.boolean = r'^true|false$'
        self.type = r'^(int|string|bool)$'

        # associative array for second types of checking
        self.regExp = { 'var': self.var, 'label': self.label, 'int': self.integer, 'string': self.string, 'bool': self.boolean, 'type': self.type }

        # creating predefined instructions
        # praca s ramci, volani funkcii
        self.move = Instruction('MOVE', 2, self.varRegex, self.symbRegex)
        self.createframe = Instruction('CREATEFRAME', 0)
        self.pushframe = Instruction('PUSHFRAME', 0)
        self.popframe = Instruction('POPFRAME', 0)
        self.defvar = Instruction('DEFVAR', 1, self.varRegex)
        self.call = Instruction('CALL', 1, self.labelRegex)
        self.retI = Instruction('RETURN', 0)
        # praca s dat zasobnikom
        self.pushs = Instruction('PUSHS', 1, self.symbRegex)
        self.pops = Instruction('POPS', 1, self.varRegex)
        # aritmeticke, relacne, booleovske a konverzni instrukce
        self.add = Instruction('ADD', 3, self.varRegex, self.symbRegex , self.symbRegex)
        self.sub = Instruction('SUB', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.mul = Instruction('MUL', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.idiv = Instruction('IDIV', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.lt = Instruction('LT', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.gt = Instruction('GT', 3, self.varRegex, self.symbRegex, self.symbRegex )
        self.eq = Instruction('EQ', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.andI = Instruction('AND', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.orI = Instruction('OR', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.notI = Instruction('NOT', 2, self.varRegex, self.symbRegex)
        self.int2char = Instruction('INT2CHAR', 2, self.varRegex, self.symbRegex)
        self.stri2int = Instruction('STR2INT', 3, self.varRegex, self.symbRegex, self.symbRegex)
        # vstupne vystupne istrukcie
        self.read = Instruction('READ', 2, self.varRegex, self.typeRegex)
        self.write = Instruction('WRITE', 1, self.symbRegex)
        # praca s retacami
        self.concat = Instruction('CONCAT', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.strlen = Instruction('STRLEN', 2, self.varRegex, self.symbRegex)
        self.getchar = Instruction('GETCHAR', 3, self.varRegex, self.symbRegex, self.symbRegex)
        self.setchar = Instruction('SETCHAR', 3, self.varRegex, self.symbRegex, self.symbRegex)
        # praca s typy
        self.typeI = Instruction('TYPE', 2, self.varRegex, self.symbRegex)
        #instrukce pro rizeni toku programu
        self.label = Instruction('LABEL', 1, self.labelRegex)
        self.jump = Instruction('JUMP', 1, self.labelRegex)
        self.jumpifeq = Instruction('JUMPIFEQ', 3, self.labelRegex, self.symbRegex, self.symbRegex)
        self.jumpifneq = Instruction('JUMPIFNEQ', 3, self.labelRegex, self.symbRegex, self.symbRegex)
        # ladiace instrukcie
        self.dprint = Instruction('DPRINT', 1, self.symbRegex)
        self.breakI = Instruction('BREAK', 0)


        # associative array of instruction objects
        self.AllInstrctions = {
                            'MOVE': self.move, 'CREATEFRAME': self.createframe, 'PUSHFRAME': self.pushframe, 'POPFRAME': self.popframe,
                            'DEFVAR': self.defvar, 'CALL': self.call, 'RETURN': self.retI, 'PUSHS': self.pushs, 'POPS': self.pops, 'ADD': self.add,
                            'SUB': self.sub, 'MUL': self.mul, 'IDIV': self.idiv, 'LT': self.lt, 'GT': self.gt, 'EQ': self.eq, 'AND': self.andI, 'OR': self.orI,
                            'NOT': self.notI, 'INT2CHAR': self.int2char, 'STRI2INT': self.stri2int, 'READ': self.read, 'WRITE': self.write,
                            'CONCAT': self.concat, 'STRLEN': self.strlen, 'GETCHAR': self.getchar, 'SETCHAR': self.setchar,'TYPE': self.typeI,
                            'LABEL': self.label, 'JUMP': self.jump, 'JUMPIFEQ': self.jumpifeq, 'JUMPIFNEQ': self.jumpifneq,'DPRINT': self.dprint,
                            'BREAK': self.breakI
                        }

    
 

    
    def parseXMLFile(self, fd):

        try:
            self.mydoc = ET.parse(fd)  
        except:
            sys.stderr.write('Could not open file or bad XML format!\n')
            exit(11)


        # for checking order atrib by instruction
        self.validInstrOrder = 0 

        self.root = self.mydoc.getroot()
        # ### checking root tag
        if self.root.tag != 'program':   
            sys.stderr.write('root tag isnt valid\n')
            sys.exit(31)
        else:
            if len(self.root.attrib.keys()) > 3 :    
                sys.stderr.write('Wrong number of attributes by root tag\n')
                sys.exit(31)
            
            # checking optional atributes in root tag
            if len(self.root.attrib.keys()) == 3:
                if 'description' not in self.root.attrib:
                    sys.stderr.write('Non existing optional attribute description for root tag\n')
                    sys.exit(31)
                if 'name' not in self.root.attrib:
                    sys.stderr.write('Non existing optional attribute name for root tag\n')
                    sys.exit(31)
            elif len(self.root.attrib.keys()) == 2:
                if 'description' not in self.root.attrib and 'name' not in self.root.attrib:
                    sys.stderr.write('Non existing optional attribute description or name for root tag\n')
                    sys.exit(31)
            # checking allowed atributes in root tag and its value
            if 'language' not in self.root.attrib:
                sys.stderr.write('Non existing required attribute for root tag\n')
                sys.exit(31)
            elif self.root.attrib['language'] != 'IPPcode18':
                sys.stderr.write('Wrong value of root tag attribute\n')
                sys.exit(31)  

            # ### checking instruction tags
            for instruction in self.root:
            
                if instruction.tag != 'instruction':
                    sys.stderr.write('Wrong tag for instruction tag\n')
                    sys.exit(31)
                else:
                    # checking  number of attributes in instruction tag
                    self.validInstrOrder = self.validInstrOrder + 1
                    if len(instruction.attrib.keys()) != 2: 
                        sys.stderr.write('Wrong number of attributes for instruction tag\n')
                        sys.exit(31)
                    else:
                        # checking allowed attributes in instruction tag
                        if 'order' not in instruction.attrib or 'opcode' not in instruction.attrib:
                            sys.stderr.write('Non existig required attributes for instrucion tag\n')
                            sys.exit(31)
                        else:
                            # checking instruction atribute order value
                            if instruction.attrib['order'] == '' or int(instruction.attrib['order']) != self.validInstrOrder:
                                sys.stderr.write('Wrong order of attributes in instruction tag' + instruction.attrib['order'] + ' ' + str(self.validInstrOrder) + '\n')
                                sys.exit(31)
                    
                    # ### checking instruction arguments tags 
                    arg1 = False
                    arg2 = False
                    arg3 = False
                    for arg in instruction:
                        # checing duplicity instr arg tag name
                        if (arg.tag == 'arg1' and arg1 == True) or (arg.tag == 'arg2' and arg2 == True) or (arg.tag == 'arg3' and arg3 == True):
                            sys.stderr.write('Duplicity in intruction argument tag name\n')
                            sys.exit(31) 
                        else:
                            if arg.tag == 'arg1':
                                arg1 = True
                            elif arg.tag == 'arg2':
                                arg2 = True
                            elif arg.tag == 'arg3':
                                arg3 = True
                            else:
                                sys.stderr.write('Wrong instruction argument tag name\n')
                                sys.exit(31)

                            # checking arg attributes number
                            if len(arg.attrib.keys()) != 1:    
                                sys.stderr.write('Wrong number of attributes in first argument tag\n')
                                sys.exit(31)
                            # checking arg attributes name
                            elif 'type' not in arg.attrib:
                                sys.stderr.write('Wrong attribute name in first argument tag\n')
                                sys.exit(31)


        # ##### save all instruction to instruction list
        self.InstrList = []  

        for instruction in self.root.findall('instruction'):
            instrName = instruction.attrib['opcode'].upper()
            i = 0
            tempArgsType = ['1', '1', '1']
            tempArgs = ['1', '1', '1']
            for args in instruction:
                if args.tag == 'arg1':
                    tempArgs[0] = args.text
                    tempArgsType[0] = args.attrib['type']
                if args.tag == 'arg2':
                    tempArgs[1] = args.text
                    tempArgsType[1] = args.attrib['type']
                if args.tag == 'arg3':
                    tempArgs[2] = args.text
                    tempArgsType[2] = args.attrib['type']
                i = i + 1

            tempInst = Instruction(instrName, i, tempArgs[0], tempArgs[1], tempArgs[2])
            tempInst.argsType(tempArgsType)
            self.InstrList.append(tempInst)


        # ##### checking all instructions from valid instruction associative array
        i = 0
        while i < len(self.InstrList):
            
            if self.InstrList[i].name.upper() not in self.AllInstrctions:
                sys.stderr.write('\n\nInstruction ' + self.InstrList[i].name + ' isnt valid\n')
                sys.exit(32)
            else:
                if self.InstrList[i].len != self.AllInstrctions[self.InstrList[i].name].len:
                    sys.stderr.write('Instruciton has wrong number of arguemnts\n')
                    sys.exit(32)
                    
                else:
                    j = 0
                    while j < self.InstrList[i].len:
                        # '1' because of my stupid solution for different but right order for instruction arguemnts
                        if self.InstrList[i].argsType[j] == '1':
                            sys.stderr.write('Nespravne poradie arguemntov')
                            sys.exit(31)

                        if  re.search(self.AllInstrctions[self.InstrList[i].name].args[j], self.InstrList[i].argsType[j]):
                            # checking for empty values in literals to set default
                            if not self.InstrList[i].args[j]:
                                if self.InstrList[i].argsType[j] == 'int':
                                    self.InstrList[i].args[j] = '0' 
                        
                                elif self.InstrList[i].argsType[j] == 'bool':
                                    self.InstrList[i].args[j] = 'true'   
                    
                                elif self.InstrList[i].argsType[j] == 'string':
                                    self.InstrList[i].args[j] = '' 
                                # empty value is of type var or label
                                else:
                                    sys.stderr.write("Empty argument for var or label: " + str(self.InstrList[i].args[j]) + self.InstrList[i].argsType[j])
                                    sys.exit(32)
                        
                            elif re.search(self.regExp[self.InstrList[i].argsType[j]], self.InstrList[i].args[j]) is None:
                                sys.stderr.write("Wrong argument " + str(self.InstrList[i].args[j]))
                                sys.exit(32) # SYNTAX ERROR wrong data type of argument value

                        else:
                            sys.stderr.write("Wrong type for argument " + self.AllInstrctions[self.InstrList[i].name].args[j] + ' ' + str(self.InstrList[i].argsType[j]))
                            sys.exit(32) # SYNTAX ERROR wrong type attribute value
                        j = j + 1
            i = i + 1


    def getInstrLis(self):
        """ return fulfiled intruction list """
        return self.InstrList